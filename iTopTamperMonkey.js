// ==UserScript==
// @name         usability hacks iTop
// @namespace    danalfvar@gmail.com
// @version      0.3
// @description  usability hacks iTop
// @author       El 🐻
// @match        */itop/pages/UI.php?*class=UserRequest*
// @grant        none
// @homepageURL  https://gitlab.com/osogtulak/itop-tampermonkey-usability-script/
// @downloadURL  https://gitlab.com/osogtulak/itop-tampermonkey-usability-script/-/raw/main/iTopTamperMonkey.js?inline=false
// @updateURL    https://gitlab.com/osogtulak/itop-tampermonkey-usability-script/-/raw/main/iTopTamperMonkey.js?inline=false
// @run-at       document-idle
// ==/UserScript==

(function() {
    'use strict';
        var dl = document.location.href;
		var lang = (navigator.language || navigator.userLanguage).split("-")[0];
		var i18n = {
			"en":{
				"create_work_order":"Create Work Order"
			},
			"es":{
				"create_work_order":"Crear orden de Trabajo"
			}
		};

		lang = (i18n[lang]|| false)?lang:'en';

        var gets = {};
        var ots = document.querySelector('.hilite');
        dl.split("?")[1].split('&').map(get=>gets[get.split("=")[0]]=get.split("=")[1]);
        if (
            typeof gets.id != "undefined" &&
            typeof gets.operation != "undefined" &&
            gets.operation == "details" &&
            !document.querySelector("#btnAddOT") &&
            !document.getElementsByName('auth_pwd').length &&
            !document.getElementsByName('auth_user').length
        ){
            ots.innerHTML = ots.innerHTML + `&nbsp;&nbsp;<div class="actions_button" id="btnAddOT"><a href="${document.location.protocol}//${document.location.hostname}/itop/pages/UI.php?operation=new&amp;class=WorkOrder&amp;c[menu]=WelcomeMenuPage&amp;default[ticket_id]=${gets.id}&amp;default[org_id]=114">${i18n[lang]['create_work_order']}</a></div>`;
        };
})();
